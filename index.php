<?php get_header();?>
    <section id="main">
        <?php if(has_nav_menu('select_menu')) { ?>
        <select name="page-dropdown" id="select_menu" onchange='document.location.href=this.options[this.selectedIndex].value;'>
           <option value=""><?php echo esc_attr( __( 'Wybierz miasto' ) );?></option>
            <?php 

                $menu_items = wp_get_nav_menu_items('select-menu');
                foreach($menu_items as $menu_item){
                    $option = '<option value="'.$menu_item->url.'">'.$menu_item->title.'</option>';
                    echo $option;
                }
            ?>
        </select>
       <?php } else {
            echo '<p>no menu select menu</p>';
        }?>
        <img src="<?php bloginfo('template_url');?>/img/strona1wlas.jpg" alt="">
    </section>
        <section id="infos" class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <img src="<?php bloginfo('template_url');?>/img/starticonpromo.png" alt="">
                    <h3>Promocje</h3>
                    <p>
                        Wchodząc na naszą stronę znajdziesz kupony rabatowe do różnych sklepów, restauracji, hoteli czy firm usługowych w wybranym mieście. Kupony należy pokazać przy kasie, aby została naliczona promocja.
                    </p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <img src="<?php bloginfo('template_url');?>/img/starticoninfo.png" alt="">
                    <h3>Informacje</h3>
                    <p>
                        Znajdziesz u nas najważniejsze informacje o zabytkach, kościołach oraz wielu innych obiektach w wybranym mieście.
                    </p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <img src="<?php bloginfo('template_url');?>/img/starticonoglo.png" alt="">
                    <h3>Ogłoszenia</h3>
                    <p>
                        Będziesz mógł wstawić własne ogłoszenia, bądź znaleźć interesujące Ciebie przedmioty u osób w Twoim mieście.
                    </p>
                </div>
            </div>
        </section>
        <footer>
            
               <div>
                   <p>&copy; copyright MC-PRODUKT</p>
               </div>
            
        </footer>
    

<?php get_footer();?>