<?php get_header();?>


    
        <div>
            <?php echo do_shortcode("[R-slider id='1']"); ?>
        </div>
        <div class="side-menu">
            <?php if(has_nav_menu('left_menu')) {
                echo '<h2>name'.$menu->name.'</h2>';
                wp_nav_menu(array('theme_location' => 'left_menu'));
    
             } else {?>
                <p>No left menu</p>
             <?php }?>
             <?php echo do_shortcode("[R-slider id='2']"); ?>
        </div>
        <div></div>
        <div>
            <?php if(has_nav_menu('right_menu')) {
                
                dynamic_sidebar( 'Main Sidebar' );
             } else {?>
                <p>No right menu</p>
             <?php }?>
        </div>
    
<?php get_footer();?>