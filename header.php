<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Document</title>
<!--    <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/magnific-popup.css">-->
    <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/bootstrap-3.3.6-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url');?>/style.css">
    <link href='https://fonts.googleapis.com/css?family=BenchNine&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    
    <?php
        wp_enqueue_script('jquery');
        wp_head();
    ?>
</head>
<body>

    <header>
        <div id="menu1">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4" id="logo">
                      <img src="<?php bloginfo('template_url');?>/img/logotaty3-1.jpg" alt="">
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-8">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </button>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" id="menu">
                       <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<?php if(has_nav_menu('header-menu')){
                            wp_nav_menu(array('theme_location' => 'header-menu'));
                        } else {
                            echo '<p>No header menu</p>';
                        } ?>
                        <a id="facebook" href="http://facebook.com" target="_blank">
                            <img src="<?php bloginfo('template_url');?>/img/1464701564_square-facebook.png" alt="">
                        </a>
                        
                        <span class="glyphicon glyphicon-search"></span>
                       </div>
                    </div>
                    
                </div>
            </div>
        </div>
        
    </header>
