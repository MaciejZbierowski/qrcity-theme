<?php

    function modify_jquery() {
if (!is_admin()) {
	wp_deregister_script('jquery');
	wp_register_script('jquery', 'https://code.jquery.com/jquery-1.11.3.min.js');
	wp_enqueue_script('jquery');
}
}
add_action('init', 'modify_jquery');

function admin_bar() {
        return false;
    }
    add_filter('show_admin_bar', 'admin_bar');

    function register_my_menus() {
        register_nav_menus(
            array(
                'header-menu' => __('Header Menu'),
                'select_menu' => __('Select Menu'),
                'left_menu' => __('Left Menu'),
                'right_menu' => __('Right Menu')
            )
        );
    }

    add_action('init', 'register_my_menus');
    
    add_action( 'widgets_init', 'theme_slug_widgets_init' );
    function theme_slug_widgets_init() {
        register_sidebar( array(
            'name' => __( 'Main Sidebar', 'theme-slug' ),
            'id' => 'sidebar-1',
            'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
            'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
        ) );
    }

?>