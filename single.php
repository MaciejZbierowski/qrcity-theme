<?php get_header();?>

<div class="container">
    <div class="row">
        <div class="col-lg-3 side-menu">
            <?php if(has_nav_menu('left_menu')) {
                $menu = wp_get_nav_menu_object('left-menu');
                echo '<h2>'.$menu->name.'</h2>';
                wp_nav_menu(array('theme_location' => 'left_menu'));
             } else {?>
                <p>No left menu</p>
             <?php }?>
        </div>
        <div class="col-lg-6"></div>
        <div class="col-lg-3 side-menu">
            <?php if(has_nav_menu('right_menu')) {
                $menu = wp_get_nav_menu_object('right-menu');
                echo '<h2>'.$menu->name.'</h2>';
                wp_nav_menu(array('theme_location' => 'right_menu'));
             } else {?>
                <p>No right menu</p>
             <?php }?>
        </div>
    </div>
</div>

<?php get_footer();?>